package sbu.cs;

import java.util.Locale;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n)
    {
        long sum = 1;
        for (int i = n ; i > 1; i--)
            sum *= i;

        return sum;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n)
    {
        if (n < 3)
        {
            long a = 1;
            return a;
        }
        else
            {
            long num = 1;
            long uppernum = 1;
            long sum = 0;
            n = n - 2;

            for (int i = n; i > 0; i--) {
                sum = num + uppernum;
                num = uppernum;
                uppernum = sum;
                sum = 0;
            }
            return uppernum;
        }
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word)
    {
        StringBuilder sb=new StringBuilder(word);
        sb.reverse();
        return sb.toString();
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {

        line = line.replaceAll("\\s+", "");
        line = line.toLowerCase(Locale.ROOT);
        StringBuilder sb = new StringBuilder(line);
        sb.reverse();
        String rev = sb.toString();
        rev = rev.replaceAll("\\s+", "");

        if (line.equals(rev)) {
            return true;
        } else {
            return false;
        }
    }
        /*
         *   implement a function which computes the dot plot of 2 given
         *   string. dot plot of hello and ali is:
         *       h e l l o
         *   h   *
         *   e     *
         *   l       * *
         *   l       * *
         *   o           *
         *   lecture 4 page 26
         */
        public char[][] dotPlot(String str1, String str2)
        {
            char[][] matrix;
            matrix = new char[str1.length()][str2.length()];

            for (int i = 0 ; i < str1.length() ; i++)
            {
                for (int j = 0 ; j < str2.length() ; j++)
                {
                    if (str1.charAt(i) == str2.charAt(j))
                    {
                        matrix[i][j] = '*';
                    }
                    else
                    {
                        matrix[i][j] = ' ';
                    }

                }
            }

            return matrix;
        }

}
