package sbu.cs;

import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length)
    {
        final char[] lowercase = "abcdefghijklmnopqrstuvwxyz".toCharArray();

        Random random = new SecureRandom();
        StringBuilder password = new StringBuilder();

        for (int i = 0; i < length; i++)
            password.append(lowercase[random.nextInt(lowercase.length)]);

        return password.toString();
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {

        if (length < 3)
            throw new IllegalValueException();

        String number = "0123456789";
        String all = "abcdefghijklmnopqrstuvwxyzABCDEFGJKLMNPRSTUVWXYZ";
        String character = "!\\\"#$%&\\'()*+,-./:;<=>?@[\\\\]^_`{|}~";

        int randomNum1 = ThreadLocalRandom.current().nextInt(0, 9);
        int randomNum2 = ThreadLocalRandom.current().nextInt(0, 35);
        int randomNum3 = ThreadLocalRandom.current().nextInt(0, 48);

        StringBuilder password = new StringBuilder();

        for (int i = 0 ;i < length - 2; i++){
            password = password.append(all.charAt(randomNum3));
        }
        password = password.append(number.charAt(randomNum1));
        password = password.append(character.charAt(randomNum2));

        return password.toString();
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n)
    {
        ExerciseLecture4 ex4 = new ExerciseLecture4();

        int i = 1;
        long fibo ;
        int binary ;

        while (true)
        {
            fibo = ex4.fibonacci(i);
            binary = Integer.toBinaryString((int)fibo).replace("0","").length() ;

            if(n == (int) fibo + binary)
                return true;
            if(n < (int) fibo)
                return false;

            i++;
        }
    }
}
