 package sbu.cs;

import java.util.List;
import java.util.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

 public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr)
    {
        long sum = 0;
        int size = arr.length;

        for (int i = 0 ; i < size ; i=i+2)
        {
                sum = sum + arr[i];
        }
        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr)
    {

        int[] rev = new int[arr.length];

        for (int i = arr.length - 1; i >= 0; i--){
            rev[arr.length - 1 - i] = arr[i];
        }
        return rev;

    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException
    {

        if (m1[0].length != m2.length) {
            throw new RuntimeException();
        }
        int Rows1 = m1.length;
        int Coulmn1 = m1[0].length;
        int Rows2 = m2.length;
        int Coulmn2 = m2[0].length;

        double[][] matrixproduct = new double[Rows1][Coulmn2];

        for(int i = 0; i < Rows1; i++)
        {
            for(int j = 0; j < Coulmn2; j++)
            {
                matrixproduct[i][j] = 0;
                for(int k = 0; k < Coulmn1; k++)
                {
                    matrixproduct[i][j] += m1[i][k] * m2[k][j];
                }
            }
        }
        return matrixproduct;


        //return null;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names)
    {
        List<List<String>> output = new ArrayList<>();

        for (int i = 0; i < names.length; i++){
            List<String> row = new ArrayList<>();

            Collections.addAll(row , names[i] );
            output.add(row);
        }
        return output;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n)
    {
        List<Integer> factors = new ArrayList<Integer>();
        for (int i = 2; i <= n; i++) {
            if (n % i == 0 )
                factors.add(i);

            while (n % i == 0)
                n /= i;
        }
        return factors;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line)
    {
        Pattern p = Pattern.compile("[a-zA-Z]+");

        Matcher m1 = p.matcher(line);
        List<String> output = new ArrayList<>();
        String temp = "";

        while (m1.find())
        {
            temp = m1.group();
            output.add(temp);
        }
        return output;
    }
}
