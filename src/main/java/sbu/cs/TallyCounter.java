package sbu.cs;

public class TallyCounter implements TallyCounterInterface {
    int counter = 0;

    @Override
    public void count()
    {
        if(-1 < counter && counter < 9999)
            counter++;
    }

    @Override
    public int getValue()
    {
        return counter;
        //return 0;
    }

    @Override
    public void setValue(int value) throws IllegalValueException {

        if(-1 < value && value < 10000)
            counter = value;
        else
            throw new IllegalValueException();
    }
}
